from django.shortcuts import render
from .models import Question
from account.models import Account

# Create your views here.
def home_screen_view(request):
	print(request.headers)
	context = {}
	# context['some_string'] = "this is some string that I'm passing to the view"

	context = {
		'some_string': "this is some string that I'm passing to the view"
	}

	context['some_number'] = 3151351

	list_of_values = []
	list_of_values.append("first entry")
	list_of_values.append("second entry")
	list_of_values.append("third entry")
	list_of_values.append("fourth entry")
	list_of_values.append("fifth entry")
	context['list_of_values'] = list_of_values

	questions =Question.objects.all()
	context['questions']=questions

	#all accounts
	accounts =Account.objects.all()
	context['accounts'] =accounts

	return render(request, "Personal/home.html", context)